JCC = javac
JAR = -classpath .:opencsv-3.3.jar

all: OscarTweets.class

OscarTweets.class: OscarTweets.java
	$(JCC) $(JAR) OscarTweets.java

clean:
	rm -f *.class

tar:
	tar cfv OscarTweeets.tar OscarTweets.java opencsv-3.3.jar \
oscar_tweets.csv Makefile
