# README #

### About This Repository ###

* Selected to participate in a proposal to Capital One for a consulting project 
(https://www.mindsumo.com/contests/351) that analyzed Twitter social media data regarding the 2015 Oscars. 
The program is a console application that reads data from a ~200k-line CSV file via the OpenCSV Java API.  
* Coded during the spring semester of my sophomore year.  
* v1.0.0  

### How To Set Up ###

* Download the project, open Terminal, and type the following commands once in the home directory of the 
project:  
1. `make`  
2. `java -classpath .:opencsv-3.3.jar OscarTweets`  

#  

* Additionaly, after running the program, type:  
1. `make tar` for a zipped copy of the project, and  
2. `make clean` to remove the generated class files as well as the zipped copy.  

#  

* **NOTE:** The **oscar_tweets.csv** file is fairly large and is intended to be opened with Microsoft Excel (opening 
it with other applications could freeze your computer), and it should always be in the root directory of 
the project.  

#### Source Files ####
* OscarTweets.java  
