//
//  OscarTweets.java
//
//
//  Created by Ross Castillo on 3/11/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

// java.util
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Comparator;
import java.util.Collections;
import java.util.LinkedHashMap;
// java.io
import java.io.FileReader;
import java.io.IOException;

// Third party library for reading/parsing the CSV file.
import com.opencsv.CSVReader;

class Time {
  public String time;
  public int count;
}

public class OscarTweets {
  public static void main(String[] args) throws IOException {
    OscarTweets obj1 = new OscarTweets();

    System.out.println();
    obj1.popularityRank();
    System.out.println();

    obj1.winnerAnnouncementPrediction();
    System.out.println();

    obj1.stateActivity();
    System.out.println();

    // For testing purposes.
    // obj1.printCSVFile();
    // System.out.println();
  }

  // Create a hash map to represent key value pairs for each film and its tweet frequency.
  // Put each film into the hash map with an initial frequency value of 0.
  //
  // Create a reader object to read the CSV file, an array to store each line's values, and skip the
  // first line of titles.
  //
  // Read through each line of the CSV file and iterate through all of the films in the hash map to
  // check if there's a match.
  //
  // If the user's tweet's text field contains a match with a film's title in the hash map, then
  // increment its count value.
  //
  // Close the reader object when finished reading the CSV file, sort the Map in descending order,
  // and print results.
  public void popularityRank() throws IOException {
    Map<String, Integer> map = new HashMap<String, Integer>();
    map.put("American Sniper", 0);
    map.put("Birdman", 0);
    map.put("Boyhood", 0);
    map.put("The Grand Budapest Hotel", 0);
    map.put("The Imitation Game", 0);
    map.put("Selma", 0);
    map.put("The Theory of Everything", 0);
    map.put("Whiplash", 0);

    CSVReader reader = new CSVReader(new FileReader("oscar_tweets.csv"), ',');
    String[] value;
    value = reader.readNext();

    while ((value = reader.readNext()) != null) {
      for (Entry<String, Integer> entry : map.entrySet()) {
        if (value[2].contains(entry.getKey())) {
          int temp = map.get(entry.getKey());
          ++temp;
          map.put(entry.getKey(), temp);
        }
      }
    }
    reader.close();

    Map<String, Integer> sortedMapDesc = sortMap(map, 2);
    System.out.println("A list of the most tweeted about best picture nominee's ranked from 1-8: ");
    printMap(sortedMapDesc, 1);
  }

  // Create an ArrayList that holds Time objects.
  //
  // Create a reader object to read the CSV file, an array to store each line's values, and skip the
  // first line of titles.
  //
  // Read through each line of the CSV file and wherever Birdman is mentioned, create a Time object
  // holding that time with an initial count value of 1 since Birdman was mentioned there, and then
  // add that object to the ArrayList. All of these times contain seconds, so the seconds have to be
  // disregarded to consider how many times Birdman was mentioned in just a particular *minute*, not
  // second.
  //
  // Iterate though the all of the time objects in the ArrayList comparing each index to the ones
  // below it (i + 1), and only below it, so duplicates are't accounted for. Subsequent indices are
  // compared only by the hour and minute characters of the time string, disregarding seconds. When
  // a match is found, the initial time object to which all subsequent indicies are being compared,
  // has its count property incremented. The subsequent index is then removed since it has now been
  // taken into account in the time object that represents all time objects to just that minute, not
  // second.
  //
  // A separate variable is needed to keep track of index in the nested for loop. J cannot be
  // incremented each time since it should always only be one past the value of i. This is because,
  // as the nested loop compares values to the initial indicies in the main loop, it removes objects
  // in the ArrayList after taking their count values into account. SO, the next object to compare
  // is always going to only be *one* past i. This is also due to the fact that all the times in the
  // CSV file are listed in chronological order. The index variable makes sure this comparison only
  // happens while still in range of the ArrayList. If j were incremented, times would be skipped.
  //
  // A different solution to this would be not removing objects after adding their count value. This
  // would allow j to be incremented for each iteration – However, the ArrayList would remain fairly
  // large and thus more expensive to perform operations with later.
  // Counting the same time object multiple times (duplicated counts) could be handled by adding
  // a bool value to the Time object (for example: bool wasChecked). Then, each object's bool value
  // could be checked before adding its count value. However, making so many checks for duplicates
  // could be time intensive since the CSV file is so large.
  //
  // An additional check is needed for the end case when the initial index tries to compare its time
  // with a subsequent index but there isn't one since it's at the end of the ArrayList now. This
  // results in multiple (two) time objects with the same time but different seconds. So, a check
  // is made to ensure the initial index is at least past the first index in the ArrayList so it can
  // check indices before the current index, and a check is made to see if the index is indeed now
  // out of range. Once that is determined, a check can be made to see if a time object with the
  // same hour and minute already exists before it. If so, the current time object's count can be
  // added to the previous time object's count value and be removed since all time objects with just
  // that hour and minute are taken into account in the previous index now.
  //
  // Find the time object in the new Arraylist that has the largest count value and then return
  // that object's hour and minute along with its tweet count value.
  public void winnerAnnouncementPrediction() throws IOException {
    ArrayList<Time> listOfTimes = new ArrayList<Time>();

    CSVReader reader = new CSVReader(new FileReader("oscar_tweets.csv"), ',');
    String[] value;
    value = reader.readNext();

    while ((value = reader.readNext()) != null) {
      if (value[2].contains("Birdman") || value[15].contains("Birdman") ||
          value[16].contains("Birdman")) {
        Time timeObj = new Time();
        timeObj.time = value[0];
        timeObj.count = 1;
        listOfTimes.add(timeObj);
      }
    }
    reader.close();

    for (int i = 0; i < listOfTimes.size(); i++) {
      int index = i + 1;  // Reset the index appropriately after each iteration.
      for (int j = i + 1; index < listOfTimes.size(); index++) {
        if (listOfTimes.get(i).time.substring(11, 16).equals(listOfTimes.get(j).time.substring(11, 16))) {

          listOfTimes.get(i).count++;
          listOfTimes.remove(j);  // Size is decremented.
          // Since size was just decremented, check if it now equals the index.
          if (index == listOfTimes.size()) break;
        }
      }
      // Satisfied at the *end* of each nested loop.
      if (i != 0 && index == listOfTimes.size()) {
        // Satisfied only when preceding time is equivalent – so only once on the final iteration.
        if (listOfTimes.get(i).time.substring(11, 16).equals(listOfTimes.get(i - 1).time.substring(11, 16))) {

          listOfTimes.get(i - 1).count += listOfTimes.get(i).count;
          listOfTimes.remove(i);
        }
      }
    }

    int maxCount = 0;
    String maxTime = "";
    for (int i = 0; i < listOfTimes.size(); i++) {
      if (listOfTimes.get(i).count > maxCount) {
        maxCount = listOfTimes.get(i).count;
        maxTime = listOfTimes.get(i).time;
      }
    }

    System.out.println("Hour and minute when Birdman was mentioned most frequenly on twitter: ");
    System.out.println(maxTime.substring(0, 16) + " PM 2015 (" + maxCount + " tweets)");
  }

  // Create a hash map to represent key value pairs for each state and its tweet frequency.
  // Put each state into the hash map with an initial value of 0.
  //
  // Create a reader object to read the CSV file, an array to store each line's values, and skip the
  // first line of titles.
  //
  // Read through each line of the CSV file and iterate though all of the states in the hash map to
  // see if there's a match.
  //
  // If the user-location-field's state matches with a state in the hash map AND the tweet contained
  // "Oscars2015", then increment its count value.
  //
  // Close the reader object when finished reading the CSV file, sort the Map in descending order,
  // and print results.
  public void stateActivity() throws IOException {
    Map<String, Integer> map = new HashMap<String, Integer>();
    map.put("AL", 0);
    map.put("AK", 0);
    map.put("AZ", 0);
    map.put("AR", 0);
    map.put("CA", 0);
    map.put("CO", 0);
    map.put("CT", 0);
    map.put("DE", 0);
    map.put("FL", 0);
    map.put("GA", 0);
    map.put("HI", 0);
    map.put("ID", 0);
    map.put("IL", 0);
    map.put("IN", 0);
    map.put("IA", 0);
    map.put("KS", 0);
    map.put("KY", 0);
    map.put("LA", 0);
    map.put("ME", 0);
    map.put("MD", 0);
    map.put("MA", 0);
    map.put("MI", 0);
    map.put("MN", 0);
    map.put("MS", 0);
    map.put("MO", 0);
    map.put("MT", 0);
    map.put("NE", 0);
    map.put("NV", 0);
    map.put("NH", 0);
    map.put("NJ", 0);
    map.put("NM", 0);
    map.put("NY", 0);
    map.put("NC", 0);
    map.put("ND", 0);
    map.put("OH", 0);
    map.put("OK", 0);
    map.put("OR", 0);
    map.put("PA", 0);
    map.put("RI", 0);
    map.put("SC", 0);
    map.put("SD", 0);
    map.put("TN", 0);
    map.put("TX", 0);
    map.put("UT", 0);
    map.put("VT", 0);
    map.put("VA", 0);
    map.put("WA", 0);
    map.put("WV", 0);
    map.put("WI", 0);
    map.put("WY", 0);

    CSVReader reader = new CSVReader(new FileReader("oscar_tweets.csv"), ',');
    String[] value;
    value = reader.readNext();

    while ((value = reader.readNext()) != null) {
      for (Entry<String, Integer> entry : map.entrySet()) {
        if (value[8].contains(entry.getKey()) && value[15].contains("Oscars2015")) {
          int temp = map.get(entry.getKey());
          ++temp;
          map.put(entry.getKey(), temp);
        }
      }
    }
    reader.close();

    Map<String, Integer> sortedMapDesc = sortMap(map, 3);
    System.out.println("States most active in tweeting about #Oscars2015 from greatest to least: ");
    printMap(sortedMapDesc, 3);
  }

  // Helper Methods

  // Sort the map of key value pairs based on the hash map's values instead of its keys.
  private static Map<String, Integer> sortMap(Map<String, Integer> unsrtMap, final int type) {
    List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsrtMap.entrySet());

    Collections.sort(list,
                     new Comparator<Entry<String, Integer>>() {
                       public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
                         if (type == 1) return o1.getValue().compareTo(o2.getValue());
                         else return o2.getValue().compareTo(o1.getValue());
                       }
                     });

    // Maintain insertion order via a LinkedList.
    Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();

    // Populate the new sorted map.
    for (Entry<String, Integer> entry : list)
      sortedMap.put(entry.getKey(), entry.getValue());

    return sortedMap;
  }

  // Print out hash map contents.
  public static void printMap(Map<String, Integer> map, int questionNum) {
    int i = 1;
    for (Entry<String, Integer> entry : map.entrySet()) {
      if (questionNum == 1) {
        System.out.println(i + ". " + entry.getKey() + " (" + entry.getValue() + " tweets) ");
      } else if (questionNum == 3)
        System.out.println(i + ". " + entry.getKey() + " ("+ entry.getValue() + " tweets)");
      ++i;
    }
  }

  // Print the contents of the CSV file to the console - For testing purposes.
  public void printCSVFile() throws IOException {
    CSVReader reader = new CSVReader(new FileReader("oscar_tweets.csv"), ',');
    String[] value;
    while ((value = reader.readNext()) != null) {
      for (int i = 0; i <= 16; ++i) {
        if (i <= 15) System.out.print(value[i] + ", ");
        else if (i == 16) System.out.print(value[i]);
      }
      System.out.println("");
    }
    reader.close();
  }
}
